#include <iostream>
#include <math.h>

using namespace std;

double getNumberOfDecimals(const double x, const int& numDecimals) {
    int y = x;
    double m = pow(10,numDecimals);
    double q = x * m;
    double r = round(q);

    return static_cast<double>(y)+(1.0/m)*r;
}

int main() {

    double input, cosineNumber;
    int numberOfIterations;

    cout << "Enter number as a double: " << endl;

    cin >> input;

    while(input != (cosineNumber = getNumberOfDecimals(cos(input), 4) ) ){
        input = cosineNumber;
        numberOfIterations++;
    }

    cout << "Dottie Number is: " << input << endl;
    cout << "Number of iterations: " << numberOfIterations << endl;

    return 0;

}